#!/bin/bash
javac -d bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls ./src/*.java

cd bin
java --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls Executable