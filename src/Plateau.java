import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class Plateau {
    
    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    private List<List<CasesInteligente>> lePlateau;

    /**
     * Constructeur de la classe Plateau.
     * @param nbLignes nombre de lignes du plateau
     * @param nbColonnes nombre de colonnes du plateau
     * @param pourcentage pourcentage de bombes sur le plateau
     */
    public Plateau(int nbLignes, int nbColonnes, int pourcentage){
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentage;
        this.creerLesCasesVides();
        this.poseDesBombesAleatoirement();
        this.rendLesCasesIntelligentes();
    }
    /**
     * Permet de créer les cases vide sur le plateau
     */
    private void creerLesCasesVides(){
        this.lePlateau = new ArrayList<>();
        for (int i=0; i<nbLignes; i++){
            this.lePlateau.add(new ArrayList<>());
            for (int j=0; j<nbColonnes; j++){
                this.lePlateau.get(i).add(new CasesInteligente());
            }
        }
    }
    /**
    * Permet de rendre les cases du plateau intelligentes.
    * Cela veut dire qu'on regarde les autres cases et qu'on les ajoute en tant que voisines.
    */
    private void rendLesCasesIntelligentes(){// veut dire qu'on regarde les autres cases et les ajoutées en voisine
        for (int i = 0; i < nbLignes; i++){
            this.lePlateau.add(new ArrayList<>());
            for(int j = 0; j < nbColonnes; j++){
                this.lePlateau.get(i).add(new CasesInteligente());
                for(int k = i-1; k <= i + 1; k++){
                    if(k < this.getNbLignes() && k >= 0){
                        for(int l = j-1; l <= j + 1; l++){
                            if(l < this.getNbColonnes() && l >= 0){
                                this.getCase(i, j).ajouteVoisine(this.getCase(k, l));
                            }
                        }  
                    }
                }

            }
        }
    }

    /**
    * Permet de poser des bombes aléatoirement sur le plateau
    * 
    */
    protected void poseDesBombesAleatoirement(){/*protected c'est que les enfants y ont acces mais pas les autres*/
        Random generateur = new Random();
        for (int x = 0; x < this.getNbLignes(); x++){
            for (int y = 0; y < this.getNbColonnes(); y++){
                if (generateur.nextInt(100)+1 < this.pourcentageDeBombes){
                    this.poseBombe(x, y);
                    this.nbBombes = this.nbBombes + 1;
                }
            }
        }
    }
    /**
     * 
     * @return
     */
    public int getNbLignes(){
        return this.nbLignes;
    }
    /** le nombre de colonnes sur le plateau
     * 
     * @return int 
     */
    public int getNbColonnes(){
        return this.nbColonnes;
    }
    /**retourne le nombre total de bombes
     * 
     * @return int
     */
    public int getNbTotalBombes(){
        return this.nbBombes;
    }
    /**retourne une case inteligente
     * 
     * @param numLigne
     * @param numColonne
     * @return CasesInteligente
     */
    public CasesInteligente getCase(int numLigne, int numColonne){
        return this.lePlateau.get(numLigne).get(numColonne);
    }
    /**Retourne le nombre de cases marquées
     * 
     * @return int
     */
    public int getNbCasesMarquees(){
        int res = 0;
        for(List<CasesInteligente> ligne : lePlateau){
            for(Case caseMarques : ligne){
                if(caseMarques.estMarquee()){
                res += 1;
                }
            } 
        }
        return res;
    }
    /**Permet de poser les bombes
     * 
     * @param x
     * @param y
     */
    public void poseBombe(int x, int y){
        this.getCase(x, y).poseBombe();
    }
    /**Permet de ralancer une partie
     * 
     */
    public void reset(){    // il faut faire un super(reset())
        this.nbBombes = 0;
        for (List<CasesInteligente> rangee : this.lePlateau){
            for (Case kase : rangee){
               kase.reset();
            }
        }
    }
}