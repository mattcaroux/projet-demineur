public class Case {
    private boolean contientUneBombe;
    private boolean estDecouverte;
    private boolean estMarquee;

    public Case(){
        /**
         * Constructeur de case
         */
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }
    /**
     * Permet de rest le plateau ainsi que le bombes
     */
    public void reset(){
        this.estDecouverte = false;
        this.estMarquee = false;
        this.contientUneBombe = false;

    }
    /**
     * Permet de poser une bombe sur le plateau
     */
    public void poseBombe(){
        this.contientUneBombe = true;
    }
    /**Permet de savoir si il y a une bombe
     * 
     * @return boolean
     */
    public boolean contientUneBombe(){
        return this.contientUneBombe;
    }
    /**Peremt de savoir si une case est decouverte ou non
     * 
     * @return
     */
    public boolean estDecouverte(){
        return this.estDecouverte;
    }
    /** Marque la case desirer
     * 
     * @return
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }
    /**Revele la case desirer
     * 
     * @return boolean
     */
    public boolean reveler(){
        if(!this.estDecouverte()){
            this.estDecouverte = true;
            return true;
        }
        return false;
    }
    /**
     * Permet de marquer une case
     */
    public void marquer(){
        if(this.estMarquee == false){
            this.estMarquee = true;
        }
        this.estMarquee = false;
    }
}
