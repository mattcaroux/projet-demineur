import java.util.List;
import java.util.ArrayList;

public class CasesInteligente extends Case{
    
    private List<Case> lesVoisines;
    
    public CasesInteligente(){
        super();
        lesVoisines = new ArrayList<>();
    }
    /**Permet d'ajouter une case inteligente cad les case qui sont a coté des notres
     * 
     * @param uneCase
     */
    public void ajouteVoisine(Case uneCase){
        this.lesVoisines.add(uneCase);
    }
    /** Permet de retourner le nombre de bombes a coter de la case
     * 
     * @return int
     */
    public int nombreBombesVoisines(){
        int compteur = 0;
        for(Case caseux: this.lesVoisines){
            if(caseux.contientUneBombe()){
                compteur += 1;
            }
        }
        return compteur;
    }
    
    @Override
    public String toString(){
        if(this.estDecouverte()){
            if(this.contientUneBombe()) return "@";
            else return ""+this.nombreBombesVoisines();
        }else{
            if(this.estMarquee())return "?";              
        }
        return " ";
    }
}
